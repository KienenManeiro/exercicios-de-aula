#include <stdio.h>
#include <stdlib.h>

int* 
alocaVetor(int a)

{
   	return (int*) malloc(sizeof(int)*a);
}


void fillVetor(int* x, int y)

{    
	for(int a=0 ; a<y ; a++)    
	scanf("%i", &x[a]);
}


void printaVetor(int* x,int y)

{
	for(int a=0 ; a<y ; a++)    
	printf("%d ", x[a]);
}


void liberaVetor(int* a)

{
 free(a);
}


int main()

{
    
int n,a=0;
int* vetorzinho;
    
printf("Manda tam de vetor: ");
scanf("%d", &n);       
vetorzinho=alocaVetor(n);
       

printf("Mande valores: ");
    

fillVetor(vetorzinho,n);
        
//for(a=0 ; a<n ; a++) 
//scanf("%d", &vetorzinho[a]);
   
 printaVetor(vetorzinho,n);
        
//for(a=0 ; a<n ; a++)
//printf("%i ", vetorzinho[a]);
    
liberaVetor(vetorzinho);
        
//free(vetorzinho);
   
 return 0;

}
