#include <stdio.h>
#include <stdlib.h>


int somatorio(int soma)
{
    if(soma==1)        return(soma);
	else			   return(soma+somatorio(soma-1));
}


int main()
{
    int n;
    printf("Digite um numero positivo: ");
    scanf("%d",&n);
    printf("O somatorio dos numeros de 1 a %d e %d.",n,somatorio(n));
    return 0;
}
