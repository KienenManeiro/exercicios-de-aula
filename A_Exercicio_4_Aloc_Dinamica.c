#include <stdio.h>
#include <stdlib.h>


int* alocaVetor(int a)
{
 return (int*) malloc(sizeof(int)*a);
}


void fillVetor(int* x, int y)
{
	for(int a=0 ; a<y ; a++)    
	scanf("%i", &x[a]);
}


void printaVetor(int* x,int y)
{
  
	for(int a=0 ; a<y ; a++)    
	printf("%d ", x[a]);
}


void liberaVetor(int* a)
{
free(a);
}


int main()
{
int n,a=0;
int* vetorzinho;
    
printf("Manda tam de vetor: ");
scanf("%d", &n);
    
vetorzinho=alocaVetor(n);//Aloca meu vetor

printf("Mande valores: ");
    

fillVetor(vetorzinho,n);//Coloca valores no vetor
        
   
printaVetor(vetorzinho,n);//Printa as casas do vetor

    
liberaVetor(vetorzinho);// D� Free no vetor
        
 return 0;

}
