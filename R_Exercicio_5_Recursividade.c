#include <stdlib.h>
#include <stdio.h>

void imprimir_pares(int n, int a)
{
    if(n==a)        printf("%d ",a);
    
	else
    {
    	printf("%d ",a);
    	imprimir_pares(n,a+2);
    }
}


int main()
{
    int num;
    printf("Entre com um numero positivo: ");
    scanf("%d",&num);
    
	if(num%2==0)    imprimir_pares(num,0);
	
	else
	{
		num-=1;
		imprimir_pares(num,0);
	}
    return 0;
}
