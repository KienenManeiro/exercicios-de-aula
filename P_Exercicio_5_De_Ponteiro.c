#include<stdio.h>
/*Mostre o inverso de uma palavra inserida pelo usuario usando ponteiros*/

void inverte(char* vetor,int* tamanho)
{
    char vetor_aux[*tamanho],aux;
    int Cont1=*tamanho;
    Cont1--;
    for(int a=0 ; a<*tamanho ; a++)
    {
        aux=vetor[Cont1];
        Cont1--;
        vetor_aux[a]=aux;
        printf("%c",vetor_aux[a]);
    }
        
}


int main()
{
	int tamanho;
	           
	printf("Digite a qntd de letras na sua palavra: \n");
	scanf("%i",&tamanho);
	char vetor[tamanho]; //Gerando vetor de char com n casas.
    printf("Digite sua palavra:  \n");
	scanf(" %s",vetor);
    
    inverte(&vetor,&tamanho);
    return 0;
}
